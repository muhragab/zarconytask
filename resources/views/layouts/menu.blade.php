

<li class="{{ Request::is('appointments*') ? 'active' : '' }}">
    <a href="{{ route('appointments.index') }}"><i class="fa fa-edit"></i><span>@lang('models/appointments.plural')</span></a>
</li>

