<div class="row">
    <!-- Start Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('start_date', __('models/appointments.fields.start_date').':') !!}
        {!! Form::date('start_date', \Carbon\Carbon::now(), ['class'=>'form-control','id'=>'start_date']) !!}
    </div>

    @push('scripts')
        <script type="text/javascript">
            $('#start_date').datetimepicker({
                format: 'Y-m-d HH:mm:ss',
                useCurrent: false
            })
        </script>
    @endpush

<!-- End Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('end_date', __('models/appointments.fields.end_date').':') !!}
        {!! Form::date('end_date', \Carbon\Carbon::now(), ['class'=>'form-control','id'=>'end_date']) !!}

    </div>

    @push('scripts')
        <script type="text/javascript">
            $('#end_date').datetimepicker({
                format: 'Y-m-d HH:mm:ss',
                useCurrent: false
            })
        </script>
@endpush

<!-- Doctor Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('doctor_id', __('models/appointments.fields.doctor_id').':') !!}
        @if(isset($appointment))
            {!! Form::select('doctor_id', \App\Models\User::where('user_type_id',1)->pluck('name', 'id'),   $appointment->doctor->id , ['class' => 'form-control']) !!}
        @else
            {!! Form::select('doctor_id', \App\Models\User::where('user_type_id',1)->pluck('name', 'id'),  null , ['class' => 'form-control']) !!}
        @endif
    </div>

    <!-- Patient Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('patient_id', __('models/appointments.fields.patient_id').':') !!}
        @if(isset($appointment))
            {!! Form::select('patient_id', \App\Models\User::where('user_type_id',2)->pluck('name', 'id'), $appointment?  $appointment->patient->id : null , ['class' => 'form-control']) !!}
        @else
            {!! Form::select('patient_id', \App\Models\User::where('user_type_id',2)->pluck('name', 'id'),  null , ['class' => 'form-control']) !!}
        @endif
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('appointments.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
    </div>

</div>
