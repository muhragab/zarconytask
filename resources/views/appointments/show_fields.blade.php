<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', __('models/appointments.fields.start_date').':') !!}
    <p>{{ $appointment->start_date }}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', __('models/appointments.fields.end_date').':') !!}
    <p>{{ $appointment->end_date }}</p>
</div>

<!-- Doctor Id Field -->
<div class="form-group">
    {!! Form::label('doctor_id', __('models/appointments.fields.doctor_id').':') !!}
    <p>{{ $appointment->doctor_id }}</p>
</div>

<!-- Patient Id Field -->
<div class="form-group">
    {!! Form::label('patient_id', __('models/appointments.fields.patient_id').':') !!}
    <p>{{ $appointment->patient_id }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/appointments.fields.created_at').':') !!}
    <p>{{ $appointment->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/appointments.fields.updated_at').':') !!}
    <p>{{ $appointment->updated_at }}</p>
</div>

