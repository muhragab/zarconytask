<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'reservation_canceled' => 'reservation has been canceled',
    'reservation_rate' => 'reservation has been rated',
    'reservation_completed' => 'reservation has been completed',
    'saved' => 'saved successfully',
    'created' => 'created successfully',
    'rejected' => 'rejected successfully.',
    'deleted' => 'deleted successfully.',
    'canceled' => 'canceled successfully.',
    'accepted' => 'accepted successfully.',
    'not_found' => 'not found .',
    'updated' => 'updated successfully.',
    'pending_status' => 'pending',
    'canceled_status' => 'canceled',
    'completed_status' => 'completed',

];
