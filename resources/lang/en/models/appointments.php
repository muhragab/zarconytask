<?php

return array (
  'singular' => 'Appointment',
  'plural' => 'Appointments',
  'fields' => 
  array (
    'id' => 'Id',
    'start_date' => 'Start Date',
    'end_date' => 'End Date',
    'doctor_id' => 'Doctor Id',
    'patient_id' => 'Patient Id',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
