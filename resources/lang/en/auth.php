<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'app' => [
        'profile' => 'profile',
        'sign_out' => 'sign out',
        'member_since' => 'member since',
        'create' => 'create',
        'export' => 'export',
        'print' => 'print',
        'reset' => 'reset',
        'reload' => 'reload',
    ],
    'login.title' => 'login',
    'failed' => 'These credentials do not match our records.',
    'de_active' => 'These captain account is not active.',
    'login' => 'Login Successfully.',
    'refresh' => 'token has been refresh.',
    'full_name' => 'full name.',
    'confirm_password' => 'confirm password.',
    'registration.i_agree' => 'agree.',
    'registration.doctor' => 'doctor',
    'registration.patient' => 'patient',
    'registration.terms' => 'terms.',
    'registration.have_membership' => 'have membership.',
    'email' => 'email.',
    'password' => 'password.',
    'remember_me' => 'remember me.',
    'forgot_password.title' => 'reset password.',
    'forgot_password.send_pwd_reset' => 'reset password.',
    'sign_out' => 'sign out.',
    'sign_in' => 'sign in.',
    'login.forgot_password' => 'forgot password.',
    'login.register_membership' => 'register membership.',
    'logout' => 'User logged out successfully.',
    'unauthorized' => 'Unauthorized.',
    'error_logout' => 'Error logged out successfully.',
    'register' => 'sign up.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'doctor_not_active' => 'Admin review .',
    'profile_completed' => 'profile completed .',
];
