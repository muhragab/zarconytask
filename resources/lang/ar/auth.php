<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'doctor_not_active' => 'جاري المراجعه من الادمن .',
    'profile_completed' => 'تم اكمال الملف الشخصي .',
    'logout' => 'تم تسجيل الخروج .',
    'unauthorized' => 'غير مصرح برجاء تسجيل الدخول .',
    'failed' => 'لا يوجد تطابق لهذه البيانات',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'app' => [
        'profile' => 'الملف الشخصي',
        'sign_out' => 'تسجيل الخروج',
        'member_since' => 'عضو منذ',
        'create' => 'انشاء',
        'export' => 'اخراج',
        'print' => 'طباعه',
        'reset' => 'reset',
        'reload' => 'تحديث البيانات',
    ],
    'login.title' => 'تسجيل الدخول',
    'login' => 'تم تسجيل الدخول بنجاح',
    'refresh' => 'تم اعاده تنشيط المستخدم',
    'full_name' => 'الاسم.',
    'confirm_password' => 'تاكيد كلمه المرور.',
    'registration.i_agree' => 'موافق .',
    'registration.doctor' => 'دكتور',
    'registration.patient' => 'مريض',
    'registration.terms' => 'الشروط .',
    'registration.have_membership' => 'لديك عضويه.',
    'email' => 'البريد الالكتروني.',
    'password' => 'كلمه المرور.',
    'remember_me' => 'تذكرني.',
    'forgot_password.title' => 'استعاده كلمه المرور.',
    'forgot_password.send_pwd_reset' => 'ارسال استعاده كلمه المرور.',
    'sign_out' => 'تسجيل الخروج.',
    'sign_in' => 'تسجيل الدخول.',
    'login.forgot_password' => 'استعاده كلمه المرور.',
    'login.register_membership' => 'تسجيل جديد.',
    'error_logout' => 'خطآ في تسجيل الخروج.',
    'register' => 'تسجيل جديد.',
];
