<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'reservation_canceled' => 'تم الغاء الحجز',
    'reservation_rate' => 'تم التقييم الحجز',
    'reservation_completed' => 'تم انهاء الحجز',
    'saved' => 'تم الحفظ',
    'created' => 'تم الانشاء',
    'updated' => ' تم التحديث.',
    'pending_status' => 'معلق',
    'canceled_status' => 'ملغي',
    'completed_status' => 'منتهي',

];
