<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'V1\Api', 'middleware' => ['localization'], 'prefix' => 'v1'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('register', 'Auth\AuthController@register');
        Route::post('login', 'Auth\AuthController@login');
        Route::get('user/type', 'GeneralController@userType');
        Route::group(['middleware' => 'auth:sanctum'], function () {
            Route::get('profile', 'Auth\AuthController@profile');
            Route::get('logout', 'Auth\AuthController@logout');
            Route::apiResource('appointment', 'AppointmentController');
        });
    });
});


Route::resource('appointments', 'AppointmentAPIController');