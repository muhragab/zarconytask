# Zarcony Task

##### how to install  :

clone with Https from :
    
    1 : git clone https://gitlab.com/muhragab/zarconytask.git
 
    2 : cd into your project
 
    3 : composer install

    4 : cp .env.example .env
    
    5 : php artisan key:generate
    
    8 : Create an empty database for our application
    
    9 : In the .env file, add database information to allow Laravel to connect to the database
    
    10 :migrate & seed ( php artisan migrate --seed )
    
### collection 
    https://www.getpostman.com/collections/da1946981b2d4bfff60a
    
    just set your env or replace {{url}}
    
    
 



