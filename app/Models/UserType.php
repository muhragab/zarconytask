<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserType extends Model
{
    use SoftDeletes;

    protected $table = 'user_types';

    protected $fillable = ['type'];

    protected $hidden = ["created_at", "updated_at", "deleted_at"];
}
