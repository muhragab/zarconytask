<?php

namespace App\Http\Controllers\V1\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppointmentRequest;
use App\Http\Requests\AppointmentUpdateRequest;
use App\Http\Resources\AppointmentResource;
use App\Repositories\AppointmentRepository;
use App\Repositories\UserRepository;
use App\Traits\PaginationTrait;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    use PaginationTrait;
    /**
     * @var AppointmentRepository
     */
    protected $appointmentRepository, $userRepository;

    /**
     * AppointmentController constructor.
     * @param AppointmentRepository $appointmentRepo
     */
    public function __construct(AppointmentRepository $appointmentRepo, UserRepository $userRepo)
    {
        $this->appointmentRepository = $appointmentRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $appointment = $this->appointmentRepository->allQuery()->where(function ($q) {
            if (request('filter') == 'next')
                $q->whereDate('start_date', '>=', Carbon::today()->toDateString());
            elseif (request('filter') == 'past')
                $q->whereDate('start_date', '<=', Carbon::today()->toDateString());
        })->paginate(15);
        $appointmentPaginated = $this->custom_paginate(AppointmentResource::collection($appointment));
        return $this->sendResponse($appointmentPaginated, '');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AppointmentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function store(AppointmentRequest $request)
    {
        $input = $request->all();
        $appointment = $this->appointmentRepository->create($input);
        return $this->sendResponse(new AppointmentResource($appointment), trans('messages.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $appointment = $this->appointmentRepository->find($id);
            return $this->sendResponse(new AppointmentResource($appointment), '');
        } catch (\Exception $e) {
            return $this->sendError(trans('messages.not_found'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AppointmentUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function update(AppointmentUpdateRequest $request, $id)
    {
        try {
            $input = array_filter($request->all());
            $appointment = $this->appointmentRepository->update($input, $id);
            return $this->sendResponse(new AppointmentResource($appointment), trans('messages.updated'));
        } catch (\Exception $e) {
            return $this->sendError(trans('messages.not_found'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $appointment = $this->appointmentRepository->delete($id);
            return $this->sendResponse('', trans('messages.deleted'));
        } catch (\Exception $e) {
            return $this->sendError(trans('messages.not_found'));
        }
    }

}
