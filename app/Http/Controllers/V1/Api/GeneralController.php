<?php

    namespace App\Http\Controllers\V1\Api;

use App\Http\Controllers\Controller;
use App\Repositories\UserTypeRepository;

class GeneralController extends Controller
{
    /**
     * @var UserTypeRepository
     */
    protected $userTypeRepository;

    /**
     * GeneralController constructor.
     * @param UserTypeRepository $userTypeRepo
     */
    public function __construct(UserTypeRepository $userTypeRepo)
    {
        $this->userTypeRepository = $userTypeRepo;
    }

    public function userType()
    {
        $userType = $this->userTypeRepository->all();
        return $this->sendResponse($userType, '');
    }

}
