<?php

namespace App\Http\Middleware;

use Closure;

class localization
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // get user and determine localizaton
        if ($request->header('lang')) $local = $request->header('lang');
        else $local = 'ar';
        // set laravel localization
        app()->setLocale($local);
        // continue request
        return $next($request);
    }
}
