<?php

namespace App\Http\Requests;

use App\Custom\CustomFormArrayRequest;

class RegisterRequest extends CustomFormArrayRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email'=>'required|email|unique:users,email',
            'password'=>'required',
            'user_type_id'=>'required|exists:user_types,id'
        ];
    }
}
