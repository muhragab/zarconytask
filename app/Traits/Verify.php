<?php

namespace App\Traits;

trait Verify
{
    use ResponseCustom;

    public function verify($model, $user, $key)
    {

        $code = mt_rand(1000, 9999);
        $model->allQuery()->updateOrCreate(
            [
                $key => $user->id,
            ],
            [
                $key => $user->id,
                'code' => $code,
            ]
        );

        return $this->sendResponse(['code' => $code, 'is_verify' => 'not_verified', 'is_completed' => $user->profile ? true : false], '');
    }

    public function verifyCode($model, $request, $relateName)
    {
        $code = $model->allQuery(['code' => $request->code])->first();
        $code[$relateName]->update([
            'is_verify' => 'verified',
        ]);

        $code->delete();
        return $code[$relateName];
    }
}
