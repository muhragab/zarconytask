<?php

namespace App\Traits;

trait UploaderTrait
{

    /**
     * @param $request
     * @param $data
     * @return mixed
     */
    public function UploadImageUpdate($request, $data)
    {
        if ($data['data']) {
            if ($data['oldPath'] = '') {
                $deleteUrl = str_replace(asset($data['path']), public_path($data['path']), $data['oldPath']);
                unlink($deleteUrl);
            }
            $file = $data['data'];
            $image = time() . '.' . $file->getClientOriginalExtension();
            $data['data']->move(public_path($data['path']), $image);
            $avatar = $data['path'] . $image;
            $Update = $data['model'] != '' ? $data['model']->update([
                $data['keyName'] => $avatar,
            ], $data['id']) : $avatar;
        }
        if (isset($Update)) {
            return $Update;
        } else {
            return 'error';
        }
    }

    public function uploadImage($request, $fileName)
    {
        if ($request->hasFile($fileName)) {
            $file = $request->file($fileName);
            $image = time() . '.' . $file->getClientOriginalExtension();
            $request[$fileName]->move(public_path('image/' . $fileName . '/'), $image);
            return $image = 'image/' . $fileName . '/' . $image;
        }
    }

    public function uploadImageWithoutRequest($sendImage, $fileName)
    {
        if ($sendImage) {
            $file = $sendImage;
            $image = time() . '.' . $file->getClientOriginalExtension();
            $sendImage->move(public_path('image/' . $fileName . '/'), $image);
            return $image = 'image/' . $fileName . '/' . $image;
        }
    }
}
