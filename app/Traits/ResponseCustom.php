<?php

namespace App\Traits;


use Response;

trait ResponseCustom
{
    public function sendResponse($result, $message)
    {
        return Response::json($this->makeResponse($message, $result));
    }

    public function sendError($message, array $error = [], $code = 400)
    {
        return Response::json($this->makeCustomError($message, $error), $code);
    }

    /**
     * @param string $message
     * @param mixed $data
     *
     * @return array
     */
    public static function makeResponse($message, $data)
    {
        return [
            'success' => true,
            'data' => $data,
            'message' => $message,
        ];
    }

    /**
     * @param string $message
     * @param array $error
     * @param array $data
     *
     * @return array
     */
    protected function makeCustomError($message, $error, array $data = [])
    {
        $res = [
            'success' => false,
            'message' => $message,
        ];

        if (!empty($data)) {
            $res['data'] = $data;
        }

        if (!empty($error)) {
            $res['error'] = $error;
        }
        return $res;
    }

}
