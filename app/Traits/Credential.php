<?php

namespace App\Traits;

use Exception;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;

trait Credential
{
    use ResponseCustom;

    protected function checkCredentials($request)
    {
        if (is_numeric($request->get('phone')) == true) {
            $credentials = ['phone' => $request->get('phone'), 'password' => $request->get('password')];
        } else {
            $credentials = ['email' => $request->get('phone'), 'password' => $request->get('password')];
        }
        return $credentials;
    }

    protected function checkActive($user)
    {
       return $user->is_active == 'active' ? true : false;
    }
}
