<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 2020-09-15
 * Time: 10:39
 */

namespace App\Traits;

trait NotificationTrait
{
    /**
     * @return mixed
     */
    protected function authKey()
    {
        return env('FB_AUTH_TOKEN');
    }

    /**
     * @param $title
     * @param $body
     * @param $topics
     * @param $type
     * @param null $channelName
     * @param null $channelChatName
     * @param null $data
     */
    public function broadCastNotification($title, $body, $topics, $type, $channelName = null, $channelChatName = null, $data = null)
    {
        $auth_key = $this->authKey();
        $topic = $topics;
        $data = [
            'body' => $body,
            'title' => $title,
            'data' => $data,
            'type' => $type,
            'channel_name' => $channelName,
            'channel_chat_name' => $channelChatName,
            'icon' => 'myicon',
            'banner' => '1',
            'sound' => 'mySound',
            "priority" => "high",
            "additional" => "additional",
        ];

        $notification = [
            'body' => $body,
            'title' => $title,
            'data' => $data,
            'type' => $type,
            'icon' => 'myicon',
            'banner' => '1',
            'sound' => 'mySound',
            "priority" => "high",
            "additional" => "additional",
        ];
        $fields = json_encode([
            'to' => $topic,
            'notification' => $notification,
            'data' => $data,
        ]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: key=' . $auth_key, 'Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        curl_exec($ch);
        curl_close($ch);
    }

}
