<?php

namespace App\Custom;


use App\Traits\ResponseCustom;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CustomFormArrayRequest extends FormRequest
{
    use ResponseCustom;
    /**
     * Validation Failed.
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->sendError( $validator->errors())                         );

    }

}
